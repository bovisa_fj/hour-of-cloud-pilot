# Hour of Cloud Pilot

This Repo contains the files and instructions for the Hour of Cloud Pilot Activity. This activity will guide you through the creation of a serverless function that used AWS services. 

All the instructions are in the [word document](Hour%20of%20cloud%20pilot%20activity.docx) or browser based [here](hour-of-cloud-pilot-activity.md) and all files required are in the [resources folder](resources).

The files contain code that should be accessed by selecting "raw" which allows for easier copying nad pasting! 

Good luck! 
