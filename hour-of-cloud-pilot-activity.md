# Hour of cloud pilot activity

Welcome to the Hour of Cloud pilot activity. We will using cloud based
serverless

technology along with other AWS services to create a simple referral
form. This is designed to be a quick demo giving a feel of the
capability of cloud services.

In summary we will create a:

-   Lambda function
-   API gateway
-   Set up SES
-   S3 static webpage

## Before you Start

You will need access to the following:

1.  https://console.aws.amazon.com
2.  http://franjilab.emeia.fujitsu.local/ParlowN/hour-of-cloud-pilot

You will also need to have an AWS free tier account created.

This activity requires the use of the AWS SES (Simple Email Service)
which is, as of writing, only available in the following regions. Make
sure you are in the correct region before starting!

|  **Region Name**        |
|-------------------------|
|  US East (N. Virginia)  |
|  US West (Oregon)       |
|  EU (Ireland)           |

**Remember to delete your project when you are finished to ensure you
are not charged!**

All the files you need are in the Franjilab project. Be sure to open the
files by clicking on "Raw"

![](media/image1.png)

## Create a lambda function

Open the AWS console, click on services and search for lambda:

![](media/image2.png)

Click on "Create Function". Click on "blueprint" search for hello-world,
select the hello-world nodejs as shown in below screenshot. Click
"Configure".

![](media/image3.png)

Give your function a name, and tell AWS to create a new role. Paste the
name in a notepad file-- you will need it later.

Enter a function name and select 'Create a new role with basic Lambda
permission'. Click "Create Function".

![](media/image4.png)

To create your AWS Lambda function, select the "edit code inline"
setting, which will have an editor box with the code in it. Replace all
the code with the contents in function.js on from the repository.

Make sure to update with your email address.

![](media/image5.png)

Scroll down to the execution role section:

![](media/image6.png){

Click the blue hyperlink to view the role:

![](media/image7.png)

Click "add inline policy" on the right hand side, click on the JSON tab
and add the code from seslambdarole.json:
![](media/image8.png)

Click "review", give it a name:

![](media/image9.png)

and then click create.

You should see this:

![](media/image10.png)

## Creating the API Gateway


Now, let's create the API Gateway that will provide a restful API
endpoint for our AWS Lambda function, which we are going to create next.
We will use this API endpoint to post user-submitted information in the
"Contact Us" form --- which will also get posted to the AWS Lambda
function.

Return to the AWS console and select API Gateway.

![](media/image11.png)

Click on create new API and fill your API name.

![](media/image12.png)

Now go to your API name --- listed in the left-hand navigation --- click
on the "Actions" drop down, and select "Create Resource". Add a resource
name such as "contact-us".

![](media/image13.png)

Select your newly-created resource and choose "Create Method." Choose
POST from the drop down and hit the tick.

![](media/image14.png)

Here, you will choose our AWS Lambda Function. To do this, type the name
of your lambda function.

![](media/image15.png)

Click OK to the permissions warning:

![](media/image16.png)

You should see this:

![](media/image17.png)

Make sure to Enable CORS in the API Gateway or you'll get an
error:"Cross-Origin Request Blocked: The Same Origin Policy disallows
reading the remote resource
at [https://abc1234.execute-api.us-east-1.amazonaws.com/02/mailme]{.underline}.
(Reason: CORS header 'Access-Control-Allow-Origin' missing)."

![](media/image18.png)

![](media/image19.png)

Accept the warning:

![](media/image20.png)

Click on the 'action' menu and choose 'Deploy API.'  Create a 'New
Stage' **After making any change to the API, you need to deploy it
again.**

![](media/image21.png)

You will see final resources and methods something like
below:![](media/image22.png)

Now get your Restful API URL from the "stages" tab as shown in the
screenshot below. We will use this URL on our "contact us" HTML page to
send the request with all user information.

![](media/image23.png)

## Setup Amazon SES

Amazon SES requires that you verify your identities (the domains or
email addresses that you send email from) to confirm that you own them,
and to prevent unauthorized use. Follow the steps outlined in
the [Amazon SES user guide ]{.underline}to verify your sender e-mail.

![](media/image24.png)

![](media/image25.png)

![](media/image26.png)

![](media/image27.png)

![](media/image28.png)

## Create your S3 website

On the AWS console, search for S3 and 'Create Bucket'.

![](media/image29.png)

![](media/image30.png)

Click on Bucket > Properties. Choose Static website hosting:

![](media/image31.png)

Type index.html in the index document box. Copy the endpoint address and
paste it into a notepad document.

![](media/image32.png)

Click on Permissions \> Public access settings \> Edit, and untick
everything. Hit Save.

![](media/image33.png)

Type confirm:

![](media/image34.png)

Now update the policy on the bucket -- click on "bucket policy", and
copy and paste the JSON code in "bucketpolicy.json". You will need to
replace the highlighted portion with the resource name given after
"Bucket Policy Editor". Don't remove the "/\*" from the end. After
clicking "save" you will see a warning about public access being
enabled.

![](media/image35.png)

Select CORS configuration. Add the contents of corsconfig.xml and click
Save.

![](media/image36.png)

Go back to your bucket overview, and click "get started" to upload a
file.

![](media/image37.png)
Copy the content from index.html on franjilab and paste it into a
notepad document. Save the document as index.html. make sure you haven't
saved it as index.html.txt. drag and drop index.html onto the upload
section of the console

![](media/image38.png)

It should look like this:

![](media/image39.png)

Click next.

Now browse to the endpoint you saved in the notepad doc. You should see
something like this:

![](media/image40.png)

Great- you've created a serverless website! Now test it...

With a bit of luck, you'll see this appear in your inbox...

![](media/image41.png)
