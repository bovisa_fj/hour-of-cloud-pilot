# Hour of Cloud Pilot Clean Up 

So you've had your fun and managed to create your serverless application. Now time to clean up your AWS account! There are 3 steps:

- [Hour of Cloud Pilot Clean Up](#hour-of-cloud-pilot-clean-up)
  - [Delete your S3 bucket](#delete-your-s3-bucket)
  - [Delete API Gateway](#delete-api-gateway)
  - [Delete Lambda Function](#delete-lambda-function)

## Delete your S3 bucket

Open the AWS Console (https://console.aws.amazon.com) and search for S3

![](/media/media-clean-up/image01.png)

This should show your S3 bucket that you created. Click the tick box for the bucket you want to delete and hit Delete.

![](/media/media-clean-up/image02.png)

Once you click Delete you will get a pop up asking you to confirm deletion. It will ask you to type the name of the bucket.

![](/media/media-clean-up/image03.png)

## Delete API Gateway

Click on Services in the top left and search for API Gateway

![](/media/media-clean-up/image04.png)

This will give you a list of existing API's, select the API that you created

![](/media/media-clean-up/image05.png)

Click on Actions and from the dropdown list click Delete API

![](/media/media-clean-up/image06.png)

You will be asked to confirm deletion by entering your API name. Click Delete API

![](/media/media-clean-up/image07.png)

## Delete Lambda Function

Click on Services in the top left and search for Lambda. This will open up your list of Lamda functions. Click on the function you want to delete > Actions > Delete.

![](/media/media-clean-up/image08.png)

You will be asked to confirm deletion:

![](/media/media-clean-up/image09.png)

All done! 